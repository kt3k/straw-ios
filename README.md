# straw-ios v0.0.1 (wip)

[![Build Status](https://travis-ci.org/strawjs/straw-ios.svg)](https://travis-ci.org/strawjs/straw-ios)
[![Coverage Status](https://coveralls.io/repos/strawjs/straw-ios/badge.png)](https://coveralls.io/r/strawjs/straw-ios)
[![MIT License](http://img.shields.io/badge/License-MIT-red.svg)]()

> Straw, the simpler native-webview bridge, for iOS (wip)

## TODO

- implementation
- cocoapods integration
